<?php

/**
 * @file
 *   Administrative forms for Evernote Site Memory module.
 */

/**
 * Build the admin settings form.
 */
function evernote_site_memory_admin_form() {
  drupal_add_css(
  	'.form-item-evernote-sitememory-button-style img { vertical-align: middle; }',
    array('type' => 'inline')
  );

  $form = array();

  $form['evernote_site_memory_node_types'] = array(
  	'#title' => t('Content types'),
    '#type' => 'checkboxes',
    '#options' => node_type_get_names(),
  	'#required' => TRUE,
  	'#default_value' => variable_get('evernote_site_memory_node_types', array()),
  );

  $form['evernote_site_memory_button_style'] = array(
    '#title' => t('Button style'),
    '#type' => 'radios',
    '#options' => _evernote_site_memory_button_style(),
    '#required' => TRUE,
    '#default_value' => variable_get('evernote_site_memory_button_style', 'clipper'),
  );

  $form['evernote_site_memory_alt_text'] = array(
    '#title' => t('Link alternate text'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('evernote_site_memory_alt_text', 'Clip to Evernote'),
  );
  
  $form['evernote_site_memory_suggested_notebook'] = array(
    '#title' => t('Suggested notebook'),
    '#type' => 'textfield',
    '#description' => t('The name of the notebook to include this note in by default.'),
    '#default_value' => variable_get('evernote_site_memory_suggested_notebook', ''),
  );

  $form['evernote_site_memory_code'] = array(
    '#title' => t('Evernote Affiliate program referral code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('evernote_site_memory_code', ''),
  );

  return system_settings_form($form);
}
